// Initialize the chat
//fbChat.bootChat();

// Procedural way
/**
 * Add a new chat message
 * 
 * @param {string} message
 */
function send_message(message,msid,sesid) {
  $.ajax({
    url: 'ajax/add_msg.php',
    method: 'post',
    data: {msg: message,msessid: msid,msesid: sesid},
    success: function(data) {
      $('#chatMsg').val('');
      get_messages(msid,sesid);
    }
  });
}

/**
 * Get's the chat messages.
 */
function get_messages(msid,sesid) {
	console.log(msid);
	console.log(sesid);
  $.ajax({
    url: 'ajax/get_messages.php?usid='+msid+'&ssid='+sesid,
    method: 'GET',
    success: function(data) {
      $('.msg-wgt-body').html(data);
    }
  });
}

/**
 * Initializes the chat application
 */
function boot_chat() {
  var chatArea = $('#chatMsg');
  var sid = $('#sid').val();
  var sessid = $('#sessid').val();
  // Load the messages every 5 seconds
  setInterval(function(){get_messages(sid,sessid);}, 20000);

  // Bind the keyboard event
  chatArea.bind('keydown', function(event) {
    // Check if enter is pressed without pressing the shiftKey
    if (event.keyCode === 13 && event.shiftKey === false) {
      var message = chatArea.val();
      // Check if the message is not empty
      if (message.length !== 0) {
        send_message(message,sid,sessid);
	//	get_messages(msid);
        event.preventDefault();
      } else {
        alert('Provide a message to send!');
        chatArea.val('');
      }
    }
  });
}

// Initialize the chat
boot_chat();
