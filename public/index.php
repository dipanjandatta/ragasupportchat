<?php
    require_once __DIR__ . '/../core/FbChatMock.php';
    $chat = new FbChatMock();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
    <!--    <link href="/style/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/style/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />-->
    <link href="style/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
	<link href="style/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--    <link href="/style/non-responsive.css" rel="stylesheet" type="text/css" />-->
    <link href="style/core.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
	<div class="col-md-12" style="padding: 0 !important;">
<div class="col-md-12 headr">
	<ul class="headnots">
		<li class="headlab">
			<a href="logout.php?un=<?php echo $_GET['mname']; ?>" class="colwhite">
				LOG OUT from Chat Support
			</a>
		</li>
		<?php if($_GET['us'] == 'sadmin') { ?>
		<li class="headlab">
		<?php
			$getMsgCount = $chat->getAdminMsgCount($_GET['mname']);
		?>
			<a href="#" class="colwhite notershow" id="<?php echo $_GET['mname']; ?>">
				Notifications<span class="counter"><?php echo $getMsgCount; ?></span>
			</a>
			<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="colwhite" style="margin-left: 10px;"><i class="fa fa-refresh"></i></a></li>
		<?php } ?>
	</ul>
	<div id="displaynotsblock" class="disphide"></div>
</div>
	</div>
    <div class="col-md-6" style="padding-top: 50px;">
    <!--      Main content area-->
	<img src="images/ci.png" style="max-width: 30%;" />
	<div class="nasha">
		You are in Chat with <?php echo $_GET['uname']; ?>
	</div>
    </div>
    <?php
    @session_start();
    
    //$_SESSION['user_id'] = isset($_GET['user_id']) ? (int) $_GET['user_id'] : 0;
    // Load the messages initially
    $messages = $chat->getMessages($_GET['sid'], $_GET['user_id']);
    ?>
<?php
if($_GET['us'] == 'sadmin') {
?>
    <div class="col-md-7">
    <div class="panel panel-default">
	<div class="panel-heading" style="font-weight: bold;font-size: 16px;">
		Raga users List
		<span class="rl">
			<a href="<?php echo $_SERVER['REQUEST_URI']; ?>">Refresh List</a>
		</span>
	</div>	
	<?php if($_GET['us'] == 'sadmin') { $ulist = $chat->getUserList($_GET['us']); 
		foreach($ulist as $userlist){
		$user_name = ucfirst($userlist['username']);
		$userid = ucfirst($userlist['id']);
		if($userlist['onstats'] == 1){
			$status = "online";
			$badgecol = "label label-success";
		} else {
			$status = "offline";
			$badgecol = "label label-danger";
		}
		echo <<<ULST
			
				<a href="index.php?user_id={$userid}&uname={$userlist['username']}&sid={$_SESSION['user_id']}&us={$_GET['us']}&mname={$_GET['mname']}" class="list-group-item">{$user_name}
				<span class="{$badgecol}" style="float:right;">
					{$status}
				</span>
</a>
ULST;
	}
}
?>
	</div>
    </div>
<?php
}
?>
    <div class="container" style="border: 1px solid lightgray;">
      <div class="msg-wgt-header">
        <a href="#"><?php echo $_GET['uname']; ?></a>
      </div>
      <div class="msg-wgt-body">
        <table>
          <?php
          if (!empty($messages)) {
            foreach ($messages as $message) {
              $msg = htmlentities($message['message'], ENT_NOQUOTES);
              $user_name = ucfirst($message['username']);
              $sent = date('F j, Y, g:i a', strtotime($message['sent_on']));
              echo <<<MSG
              <tr class="msg-row-container">
                <td>
                  <div class="msg-row">
                    <div class="message">
                      <span class="user-label"><span class="msg-time">{$sent}</span></span><br/>{$msg}
                    </div>
                  </div>
                </td>
              </tr>
MSG;
            }
          } else {
            echo '<span style="margin-left: 25px;">No chat messages available!</span>';
          }
          ?>
        </table>
      </div>
      <div class="msg-wgt-footer">
        <textarea id="chatMsg" placeholder="Type your message. Press shift + Enter to send"></textarea>
	<input type="hidden" id="sid" value="<?php echo $_GET['sid']; ?>" />
	<input type="hidden" id="sessid" value="<?php echo $_GET['user_id']; ?>" />
      </div>
    </div>
    
    <script type="text/javascript" src="scripts/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="scripts/chat.js"></script>
  </body>
</html>
