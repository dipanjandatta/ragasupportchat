<?php
	    require_once __DIR__ . '/../core/FbChatMock.php';
	    $chat = new FbChatMock();
		$insUser = $chat->addUser($_GET['source'],$_GET['un']);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
    <!--    <link href="/style/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/style/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />-->
    <link href="style/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="style/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--    <link href="/style/non-responsive.css" rel="stylesheet" type="text/css" />-->
    <link href="style/core.css" rel="stylesheet" type="text/css" />
	<link href="style/font-awesome.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
<div class="container-fluid" style="padding: 0 !important;">
<div class="col-md-12" style="padding: 0 !important;">
<div class="col-md-6 bgCol" style="padding-top: 150px;">
	<div class="heading manu">
		Welcome to Support
	</div>
	<div class="imagePart" style="text-align: center;">
		<img src="images/ragamixlatest.png" style="max-width: 50%;" />
	</div>
</div>
<div class="col-md-6 headr">
	<ul class="headnots">
		<li class="headlab">
			<a href="logout.php?un=<?php echo $_GET['un']; ?>" class="colwhite">
				LOG OUT from Chat Support
			</a>
		</li>
		<?php if($_GET['source'] == 'sadmin') { ?>
		<li class="headlab">
		<?php
			$getMsgCount = $chat->getAdminMsgCount($_GET['un']);
		?>
			<a href="#" class="colwhite notershow" id="<?php echo $_GET['un']; ?>">
				Notifications<span class="counter"><?php echo $getMsgCount; ?></span>
			</a>
			<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="colwhite" style="margin-left: 10px;"><i class="fa fa-refresh"></i></a></li>
		<?php } ?>
	</ul>
	<div id="displaynotsblock" class="disphide"></div>
</div>
<div class="col-md-6" style="padding-top: 100px;">
    <div class="panel panel-default">
      <div class="panel-heading" style="font-weight: bold;font-size: 16px;">
	<?php if($_GET['source'] == 'nuser'){ ?>
		Support Chat Admin List
	<?php } else { ?>
		Raga users List
	<?php } ?>
	<span class="rl">
		<a href="<?php echo $_SERVER['REQUEST_URI']; ?>">Refresh List</a>
	</span>
      </div>
	<?php if($_GET['source'] == 'nuser') { ?>
		<div class="panel-body">
			<p>Click on Below Admins List to start chat</p>
		</div>
	<?php } ?>
	<div class="list-group">
	<?php
	    
	    @session_start();
	    $_SESSION['user_id'] = $insUser;
	    $ulist = $chat->getUserList($_GET['source']);
		foreach($ulist as $userlist){
		$user_name = ucfirst($userlist['username']);
		$userid = ucfirst($userlist['id']);
		if($userlist['onstats'] == 1){
			$status = "online";
			$badgecol = "label label-success";
		} else {
			$status = "offline";
			$badgecol = "label label-danger";
		}
		echo <<<ULST
			
				<a href="index.php?user_id={$userid}&uname={$userlist['username']}&sid={$_SESSION['user_id']}&us={$_GET['source']}&mname={$_GET['un']}" class="list-group-item">{$user_name}
				<span class="{$badgecol}" style="float:right;">
					{$status}
				</span>
</a>


ULST;
	}
	?>
					</div>
	</div>
</div>
</div>
</div>
    <script type="text/javascript" src="scripts/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="scripts/manual.js"></script>
  </body>
</html>
