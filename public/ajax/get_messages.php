<?php

require_once __DIR__ . '/../../core/FbChatMock.php';

$chat = new FbChatMock();
$badCompany = (int) $_GET['usid'];
$badCompanyOne = (int) $_GET['ssid'];
$messages = $chat->getMessages($badCompany, $badCompanyOne);
$chat_converstaion = array();

if (!empty($messages)) {
  $chat_converstaion[] = '<table>';
  foreach ($messages as $message) {
    $msg = htmlentities($message['message'], ENT_NOQUOTES);
    //$user_name = ucfirst($message['username']);
    $sent = date('F j, Y, g:i a', strtotime($message['sent_on']));
    $chat_converstaion[] = <<<MSG
      <tr class="msg-row-container">
        <td>
          <div class="msg-row">
            <div class="message">
              <span class="user-label"><span class="msg-time">{$sent}</span></span><br/>{$msg}
            </div>
          </div>
        </td>
      </tr>
MSG;
  }
  $chat_converstaion[] = '</table>';
} else {
  echo '<span style="margin-left: 25px;">No chat messages available!</span>';
}

echo implode('', $chat_converstaion);
?>
