<?php

require_once __DIR__ . '/../../core/FbChatMock.php';

$chat = new FbChatMock();
$badCompany = $_GET['uname'];
$messages = $chat->getNotsMessages($badCompany);
?>
	<ul class="no">
<?php
foreach($messages as $message) {
?>

		<li>
		<span class="mc"><?php echo $message['username']; ?></span>
		<span class="mm">
			<?php echo $message['message']; ?>
		</span>
		<span class="mb">
			Sent on <?php echo date('d M Y', strtotime($message['sent_on'])); ?>
		</span>
		</li>

<?php
}
?>
	</ul>
