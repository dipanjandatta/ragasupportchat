<?php

/**
 * Description of FbChatMock
 *
 * @author Tamil
 */
class FbChatMock {

  // Holds the database connection
  private $dbConnection;
  
  //----- Database connection details --/
  //-- Change these to your database values
  
  private $_dbHost = '52.204.212.14';
  
  private $_dbUsername = 'ragauser';
  
  private $_dbPassword = 'rags@321#';
  
  public $_databaseName = 'fbchat';
  
  //----- ----/

  /**
   * Create's the connection to the database and stores it in the dbConnection
   */
  public function __construct() {
    $this->dbConnection = new mysqli($this->_dbHost, $this->_dbUsername, 
        $this->_dbPassword, $this->_databaseName);

    if ($this->dbConnection->connect_error) {
	echo $this->dbConnection->connect_error;
      die('Connection error.');
    }
  }

  /**
   * Get the list of messages from the chat
   * 
   * @return array
   */
  public function getMessages($bc, $badCompanyOne) {
	/*$dVil = (int) $bc;
	$dVilOne = (int) $badCompanyOne;*/
    $messages = array();
    $query = "
	SELECT 
          `chat`.`message`, 
          `chat`.`sent_on`
	 FROM `chat` 
	WHERE (`chat`.user_id = '{$bc}' and `chat`.sess_id = '{$badCompanyOne}') OR (`chat`.user_id = '{$badCompanyOne}' and `chat`.sess_id = '{$bc}')

";
    
    // Execute the query
    $resultObj = $this->dbConnection->query($query);
    // Fetch all the rows at once.
    while ($row = $resultObj->fetch_assoc()) {
      $messages[] = $row;
    }
    
    return $messages;
  }

  /**
   * Add a new message to the chat table
   * 
   * @param Integer $userId The user who sent the message
   * @param String $message The Actual message
   * @return bool|Integer The last inserted id of success and false on faileur
   */
  public function addMessage($userId, $message, $oid, $osid) {
    $addResult = false;
    //$cUserId = (int) $userId;
    $cOid = (int) $oid;
    $cOsid = (int) $osid;
    // Escape the message with mysqli real escape
    $cMessage = $this->dbConnection->real_escape_string($message);
    
    $query = <<<QUERY
      INSERT INTO `chat`(`user_id`, `message`, `sent_on`, `sess_id`, `has_read`)
      VALUES ({$cOsid}, '{$cMessage}', NOW(), {$cOid}, 0)
QUERY;

    $result = $this->dbConnection->query($query);
    
    if ($result !== false) {
      // Get the last inserted row id.
      $addResult = $this->dbConnection->insert_id;
    } else {
      echo $this->dbConnection->error;
    }
    
    return $addResult;
  }


  public function getUserList($source) {
    $ulist = array();
    $query = <<<QUERY

	select * from users where utype <> '{$source}'
QUERY;
    
    // Execute the query
    $resultObj = $this->dbConnection->query($query);
    // Fetch all the rows at once.
    while ($row = $resultObj->fetch_assoc()) {
      $ulist[] = $row;
    }
    
    return $ulist;
  }


  public function addUser($sou, $uname) {
   $queryCheck = <<<QUERY
		select * from users where username = '{$uname}' and utype = '{$sou}'
QUERY;
		$resultCheck = $this->dbConnection->query($queryCheck);
		if($resultCheck->num_rows > 0){
			$queryUp = "update users set onstats = 1 where username = '{$uname}' and utype = '{$sou}'";
			$resultUp = $this->dbConnection->query($queryUp);
			$row = $resultCheck->fetch_assoc();
			return $row['id'];
		}
		else {

		
    $query = <<<QUERY
      INSERT INTO `users`(`username`, `rmid`, `utype`, `onstats`)
      VALUES ('{$uname}', '0', '{$sou}', 1)

QUERY;

    $result = $this->dbConnection->query($query);
    
    if ($result !== false) {
      // Get the last inserted row id.
      $addResult = $this->dbConnection->insert_id;
    } else {
      echo $this->dbConnection->error;
    }
	return $addResult;
}
    
  }


	public function logoutuser($username){
		$query = <<<QUERY
				update users set onstats = 0 where username = '{$username}'
QUERY;
		$result = $this->dbConnection->query($query);
		if($result !== false) {
			return true;
		}
		else {
			echo $this->dbConnection->error;
			return false;
		}
	}

	public function getAdminMsgCount($username) {
		//print_r($username);
		$query = "select id from users where username = '{$username}'";
		$result = $this->dbConnection->query($query);
		$row = $result->fetch_assoc();
		$queryNew = "select count(*) as chcount from chat where user_id={$row['id']} and has_read = 0";
		$resultNew = $this->dbConnection->query($queryNew);
		$rowNew = $resultNew->fetch_assoc();
		return $rowNew['chcount'];
	}

	public function getNotsMessages($username) {
		    $mlist = array();
		$query = "select id from users where username = '{$username}'";
		$result = $this->dbConnection->query($query);
		$row = $result->fetch_assoc();
		$queryNew = "select a.*, b.username from chat a, users b where a.user_id = {$row['id']} and a.sess_id = b.id and a.has_read = 0";
		$resultNew = $this->dbConnection->query($queryNew);
		$queryUp = "update chat set has_read = 1 where user_id = {$row['id']}";
		$resultUp = $this->dbConnection->query($queryUp);
		    // Fetch all the rows at once.
		    while ($rowNew = $resultNew->fetch_assoc()) {
		      $mlist[] = $rowNew;
		    }
		    
		    return $mlist;
	}

}
